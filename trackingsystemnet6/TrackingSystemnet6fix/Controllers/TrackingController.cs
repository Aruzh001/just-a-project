﻿using Microsoft.AspNetCore.Mvc;
using TrackingSystem.DataAccessLayer.Entity;
using TrackingSystem.DataAccessLayer.HttpRequests;
using TrackingSystem.DataAccessLayer.DatabaseConnect;
using Npgsql;


namespace TrackingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrackingController : Controller
    {

        private readonly DatabaseConnect databaseConnect;
        //private readonly NpgsqlDataSource _datasource;

        static Random random = new Random();
        public TrackingController()
        {
            //var connectionString ="Host=asa.postgres.database.azure.com;Username=asa_admin;Password=postgres_1;Database=postgres;SSL Mode=Disable;Port=5432";
            databaseConnect = new DatabaseConnect("asa_db", "asa_admin", "postgres_1", "40.121.86.218", "5432");
            //var source = NpgsqlDataSource.Create(connectionString);
            //_datasource = source;
        }

        [HttpGet("Get_Coordinates")]
        public async Task<ActionResult> GetCoordinates()
        {
            var courier = new Courier("Placeholder", 0, 0);
            NpgsqlConnection conn = databaseConnect.Connect();
            await using var cmd = new NpgsqlCommand($"SELECT name, longitude, latitude FROM couriers_imran ORDER BY id DESC LIMIT 1;", conn);
            await using var reader = await cmd.ExecuteReaderAsync();
            reader.Read();
            
                string name = reader.GetString(0);
                double longitude = reader.GetDouble(1);
                double latitude = reader.GetDouble(2);

                courier.CourierName = name;
                courier.Latitude = latitude;
                courier.Longitude = longitude;
            
            conn.Close();
            return Ok(courier);
        }

        [HttpPost("Send_Coordinates")]
        public async Task<ActionResult> SendCoordinates()
        {
            string CourierName = ".Net Courier";
            double Longitude = GetRandomValue(71.0, 72.0, 3);
            double Latitude = GetRandomValue(51.0, 51.5, 3);

            NpgsqlConnection conn = databaseConnect.Connect();
            await using var cmd = new NpgsqlCommand("INSERT INTO couriers_imran (name, longitude, latitude) VALUES (@Name, @Longitude, @Latitude)", conn);

            cmd.Parameters.AddWithValue("Name", CourierName);
            cmd.Parameters.AddWithValue("Longitude", Longitude);
            cmd.Parameters.AddWithValue("Latitude", Latitude);

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
            return Ok("Courier Inserted");

        }

        [HttpGet("Test")]
        public async Task<ActionResult> Test()
        {
            return Ok("Test");
        }

        static double GetRandomValue(double from, double to, int fixedDigits)
        {
            double range = to - from;
            double randomValue = random.NextDouble() * range + from;
            return Math.Round(randomValue, fixedDigits);
        }
    }
}
