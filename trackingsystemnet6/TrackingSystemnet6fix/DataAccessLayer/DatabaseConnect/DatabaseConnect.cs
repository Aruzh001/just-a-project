﻿using Npgsql;

namespace TrackingSystem.DataAccessLayer.DatabaseConnect
{
    public class DatabaseConnect
    {
        private readonly string connectionString;

        public DatabaseConnect(string dbName, string userName, string password, string host, string port)
        {
            connectionString = $"Host={host};Port={port};Username={userName};Password={password};Database={dbName}";
        }

        public NpgsqlConnection Connect()
        {
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();
            Console.WriteLine("Connected to database");
            return connection;
        }

        public NpgsqlConnection Close()
        {
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Close();
            Console.WriteLine("Connection closed");
            return connection;
        }
    }
}
