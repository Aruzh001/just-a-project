﻿namespace TrackingSystem.DataAccessLayer.Entity
{
    public class Courier
    {
        public Courier(string courierName, double longitude, double latitude)
        {
            CourierName = courierName;
            Longitude = longitude;
            Latitude = latitude;
        }

        public string CourierName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
