﻿namespace TrackingSystem.DataAccessLayer.HttpRequests
{
    public class AddCoordinateRequest
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
