
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddControllers();
builder.Services.AddControllersWithViews();
builder.Services.AddRouting();
builder.Services.AddCors(p => p.AddPolicy("corspolicy", build =>
{
    build.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.MapControllers();
app.UseDeveloperExceptionPage();
app.UseSwagger();
app.UseSwaggerUI(options => {
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    options.RoutePrefix = string.Empty;
});
app.UseCors("corspolicy");
app.UseHsts();
app.UseExceptionHandler(new ExceptionHandlerOptions()
{
    //AllowStatusCode404Response = true,
    ExceptionHandlingPath = "/error"
});
void ConfigureServices(IServiceCollection services)
{
    services.AddControllers();
    services.AddControllersWithViews();
    services.AddRouting();

}

void Configure(IApplicationBuilder app)
{
}

if (app.Environment.IsDevelopment())
{
}


app.UseHttpsRedirection();
app.UseDefaultFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.MapRazorPages();

app.Run();