package com.example.javatrackingsystem.config;

import com.example.javatrackingsystem.services.handlers.CourierWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(courierWebSocketHandler(), "/websocket").setAllowedOrigins("*");
    }

    @Bean
    public CourierWebSocketHandler courierWebSocketHandler() {
        return new CourierWebSocketHandler();
    }
}

