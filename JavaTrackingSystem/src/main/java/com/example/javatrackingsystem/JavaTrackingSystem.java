package com.example.javatrackingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTrackingSystem {

    public static void main(String[] args) {
        SpringApplication.run(JavaTrackingSystem.class, args);
    }

}

