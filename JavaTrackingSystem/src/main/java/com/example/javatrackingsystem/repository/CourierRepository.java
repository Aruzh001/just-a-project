package com.example.javatrackingsystem.repository;

import com.example.javatrackingsystem.model.Courier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourierRepository extends JpaRepository<Courier, Long> {
}