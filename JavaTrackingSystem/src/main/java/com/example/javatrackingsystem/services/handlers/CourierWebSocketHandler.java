package com.example.javatrackingsystem.services.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class CourierWebSocketHandler extends TextWebSocketHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Map<WebSocketSession, Long> sessionCourierMap = new ConcurrentHashMap<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        SubscriptionMessage subscriptionMessage = objectMapper.readValue(message.getPayload(), SubscriptionMessage.class);

        if ("subscribe".equals(subscriptionMessage.getAction())) {
            sessionCourierMap.put(session, subscriptionMessage.getCourierId());
            session.sendMessage(new TextMessage("{\"status\":\"subscribed\",\"courierId\":" + subscriptionMessage.getCourierId() + "}"));
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        sessionCourierMap.remove(session);
    }

    public void sendLocationUpdate(Long courierId, String locationMessage) {
        sessionCourierMap.forEach((session, subscribedCourierId) -> {
            if (courierId.equals(subscribedCourierId)) {
                try {
                    if (session.isOpen()) {
                        session.sendMessage(new TextMessage(locationMessage));
                    }
                } catch (IOException e) {
                    log.error("Error during send location update", e);
                }
            }
        });
    }

    @Getter
    @Setter
    private static class SubscriptionMessage {
        private String action;
        private Long courierId;
    }
}
