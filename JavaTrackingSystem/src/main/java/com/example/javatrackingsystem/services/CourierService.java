package com.example.javatrackingsystem.services;

import com.example.javatrackingsystem.model.Courier;
import com.example.javatrackingsystem.repository.CourierRepository;
import com.example.javatrackingsystem.services.handlers.CourierWebSocketHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
public class CourierService {
    private final CourierWebSocketHandler courierWebSocketHandler;
    private final Map<Long, Boolean> runningTasks = new ConcurrentHashMap<>();
    private final CourierRepository courierRepository;
    private final TaskScheduler taskScheduler;
    private final Map<Long, ScheduledFuture<?>> scheduledTasks = new ConcurrentHashMap<>();

    public CourierService (CourierWebSocketHandler courierWebSocketHandler, CourierRepository courierRepository, TaskScheduler taskScheduler) {
        this.courierWebSocketHandler = courierWebSocketHandler;
        this.courierRepository = courierRepository;
        this.taskScheduler = taskScheduler;
    }

    public void startSendingLocation(Long id) {
        runningTasks.put(id, true);

        ScheduledFuture<?> existingTask = scheduledTasks.get(id);
        if (existingTask != null && !existingTask.isDone()) {
            existingTask.cancel(true);
        }

        ScheduledFuture<?> scheduledFuture = taskScheduler.scheduleWithFixedDelay(() -> sendLocationUpdates(id), 3000);
        scheduledTasks.put(id, scheduledFuture);
    }


    public void stopSendingLocation(Long id) {
        runningTasks.put(id, false);

        ScheduledFuture<?> scheduledFuture = scheduledTasks.remove(id);
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }

    public void sendLocationUpdates(Long id) {
        log.info("inside");
        if (!runningTasks.getOrDefault(id, false)) return;

        courierRepository.findById(id).ifPresent(courier -> {
            double latitude = ThreadLocalRandom.current().nextDouble(51.0, 51.5);
            double longitude = ThreadLocalRandom.current().nextDouble(71.0, 72.0);

            courier.setLatitude(String.valueOf(latitude));
            courier.setLongitude(String.valueOf(longitude));

            try {
                String message = new ObjectMapper().writeValueAsString(courier);
                courierWebSocketHandler.sendLocationUpdate(id, message);
            } catch (JsonProcessingException e) {
                log.error("Error serializing courier data", e);
            }
        });
    }



    public Courier saveNewCourier() {
        double latitude = ThreadLocalRandom.current().nextDouble(51.0, 51.5);
        double longitude = ThreadLocalRandom.current().nextDouble(71.0, 72.0);

        Courier courier = new Courier(String.valueOf(latitude), String.valueOf(longitude));
        return courierRepository.save(courier);
    }
}
