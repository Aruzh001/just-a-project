package com.example.javatrackingsystem.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "couriers_amira")
@Setter
@Getter
public class Courier {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    public Courier(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Courier() {
    }
}