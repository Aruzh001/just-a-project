package com.example.javatrackingsystem.rest;

import com.example.javatrackingsystem.model.Courier;
import com.example.javatrackingsystem.repository.CourierRepository;
import com.example.javatrackingsystem.services.CourierService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController("/api")
@RequestMapping("/courier")
public class CourierController {
    private final CourierService courierService;
    private final CourierRepository courierRepository;

    public CourierController(CourierService courierService, CourierRepository courierRepository) {
        this.courierService = courierService;
        this.courierRepository = courierRepository;
    }

    @GetMapping("/start")
    public ResponseEntity<Courier> startCourierPath() {
        Courier courier = courierService.saveNewCourier();
        courierService.startSendingLocation(courier.getId());
        return ResponseEntity.ok(courier);
    }

    @GetMapping("/stop")
    public ResponseEntity stopCourierPath(@RequestParam Long id) {
        if (courierRepository.existsById(id)) {
            courierService.stopSendingLocation(id);
            return ResponseEntity.ok("Successfully stopped");
        }
        return new ResponseEntity("Courier with this ID doesnt exist", HttpStatus.NO_CONTENT);
    }
}
