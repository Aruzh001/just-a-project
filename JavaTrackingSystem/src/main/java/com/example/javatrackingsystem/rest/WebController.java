package com.example.javatrackingsystem.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {
    @GetMapping("/track")
    public String showTrack() {
        return "track";
    }

    @GetMapping("/")
    public String main() {return "main";}
}
